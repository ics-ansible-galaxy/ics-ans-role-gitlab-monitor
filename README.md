# ics-ans-role-gitlab-monitor

Ansible role to install gitlab-monitor.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-gitlab-monitor
```

## License

BSD 2-clause
