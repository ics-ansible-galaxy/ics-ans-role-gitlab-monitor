import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_docker_image(host):
    container = "gitlab-monitor"
    image = host.run("sudo docker container ls")
    if container in image.stdout:
        assert True
